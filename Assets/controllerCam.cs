﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controllerCam : MonoBehaviour {
    float speed = 10.0f;
    public float speedDrag = 1f;
    private float X;
    private float Y;
    private float Z;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        //Vector3 v3 = new Vector3(Input.GetAxis("Vertical"), 0f, -Input.GetAxis("Horizontal"));
        //transform.Rotate(v3 * speed * Time.deltaTime);
        //if (Input.GetMouseButton(0))
        //{
        //    transform.Rotate(new Vector3(Input.GetAxis("Mouse Y") * speed, -Input.GetAxis("Mouse X") * speed, Input.GetAxis("Mouse Z") * speed));
        //    X = transform.rotation.eulerAngles.x;
        //    Y = transform.rotation.eulerAngles.y;
        //    Z = transform.rotation.eulerAngles.z;
        //    transform.rotation = Quaternion.Euler(X, Y, Z);
        //}
        if (Input.GetKey(KeyCode.A))
        {
            transform.Rotate(Vector3.forward);
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.Rotate(-Vector3.forward);
        }
        if (Input.GetKey(KeyCode.W))
        {
            transform.Rotate(Vector3.right);
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.Rotate(-Vector3.right);
        }

    }
}
