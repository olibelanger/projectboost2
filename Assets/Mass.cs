﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mass : MonoBehaviour {

    public Transform centerMass;

	// Use this for initialization
	void Start () {
        this.GetComponent<Rigidbody>().centerOfMass = centerMass.transform.position;
		
	}
	
}
