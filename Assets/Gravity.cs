﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gravity : MonoBehaviour {

    public GameObject lemObject;
    public float gravity;

	// Use this for initialization
	void Start () {
       // lemObject = lemObject.GetComponent<GameObject>();
        print(lemObject.transform.position);
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        Vector3 lemObjectPosition = lemObject.transform.position;
        float distance = Vector3.Distance(transform.position, lemObjectPosition);
        //lemObject.transform.position -= new Vector3(0f, 0.001f, 0f) ;
        lemObject.GetComponent<Rigidbody>().AddRelativeForce(Vector3.Normalize(transform.position - lemObjectPosition)* (1 / distance * distance) * gravity * Time.deltaTime);
		
	}
}
